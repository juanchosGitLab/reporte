<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{

    
    protected $table = 'N_SPECIALITIES';

    protected $fillable = [        
        'description',
        'upss',
    ];  


}
