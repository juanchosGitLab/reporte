<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    @inertiaHead
</head>

<body>
<!-- Page Content -->

    <main role="main">
        <div class="content container-fluid">
          
            @vite(['resources/js/app.js', "resources/js/Pages/{$page['component']}.vue"])
            @inertia
           
        </div>
    </main>

</body>

</html>

<script>

</script>