import './bootstrap';
import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/vue3';
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
import { ZiggyVue } from '../../vendor/tightenco/ziggy/dist/vue.m';
import { Quasar,Dialog,Notify,Loading } from 'quasar';
import '@quasar/extras/material-icons/material-icons.css';

import 'quasar/src/css/index.sass';
import quasarLang from 'quasar/lang/es';


const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => {    
       

         return resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob('./Pages/**/*.vue'));
  
       // resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob('./Pages/**/*.vue')),
    },    
    setup({ el, App, props, plugin }) {
        const VueApp = createApp({ render: () => h(App, props) });  
    
        VueApp.use(plugin)
        .use(ZiggyVue)           
        .use(Quasar, {
            plugins: {
                Dialog,
                Notify,
                Loading,
              
              },
              config: {
                notify: {  }
              },
            lang: quasarLang          
        })     
     
        .mount(el);       
    },
    progress: {
        color: '#4B5563',
    },
    
})

//);

